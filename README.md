# EPS.ExpressBootviewDataMigration

Data Migration solution using Visual Studio, SSDT (SQL Server Data Tools) and SSIS to prepare export scripts from Bootview database to a typical EPS MySQL instance.

Setup Instructions: https://paylock.atlassian.net/wiki/spaces/EN/pages/557088787/SSIS+and+Visual+Studio+Environment+Set+Up

Doing a migration with SSIS: https://paylock.atlassian.net/wiki/spaces/EN/pages/557875207/How+to+migrate+client+data+to+EPS

Migrating the MySQL tables out from local MySQL to a server (production) instance:

	SQLDataDump is located in your MySQL "bin" folder; My database instance is 'smart'
	 
	1. Open a terminal window and cd C:\Program Files\MySQL\MySQL Server 8.0\bin\ (or set a path to it)

	2. Export table schema and data for a particular table into 1 file: mysqldump -u root -p smart imp_parkingareas > parkingareas.sql

	3. For many tables into 1 file: mysqldump -u root -p smart imp_customers imp_bootreasons imp_officers imp_parkingareas imp_boots > schema.sql
